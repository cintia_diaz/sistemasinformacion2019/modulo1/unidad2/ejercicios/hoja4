﻿USE ciclistas;

/*
  HOJA 4 - Consultas de selección 4
 
  */
-- CONSULTA DE COMBINACION INTERNAS

-- 1.1 Nombre y edad de los ciclistas que han ganado etapas
SELECT DISTINCT e.dorsal FROM etapa e;

SELECT c.nombre, c.edad FROM (
  SELECT DISTINCT e.dorsal FROM etapa e
  ) c1
  JOIN 
  ciclista c
  USING (dorsal);


-- 1.2 Nombre y edad de los ciclistas que han ganado puertos
SELECT DISTINCT p.dorsal FROM puerto p;

SELECT c.nombre, c.edad FROM (
  SELECT DISTINCT p.dorsal FROM puerto p
  ) c1
  JOIN
  ciclista c
  USING (dorsal);

-- 1.3 Nombre y edad de los ciclistas que han ganado etapas y puertos
  
  -- ciclistas que han ganado etapas
  SELECT DISTINCT e.dorsal FROM etapa e;

  -- ciclistas que han ganado puertos
  SELECT DISTINCT p.dorsal FROM puerto p;

  -- ciclistas que han ganado etapas y puertos
  SELECT c.nombre, c.edad FROM (
    SELECT DISTINCT e.dorsal FROM etapa e
    ) c1
    JOIN (
    SELECT DISTINCT p.dorsal FROM puerto p
    ) c2
    USING (dorsal)
    JOIN 
    ciclista c
    USING (dorsal);

-- 1.4 Listar el director de los equipos que tengan ciclistas que hayan ganado alguna etapa

    -- ciclistas que han ganado alguna etapa
    SELECT DISTINCT e.dorsal FROM etapa e;

    -- nombre de los equipos de esos ciclistas
    SELECT DISTINCT c.nomequipo FROM (
      SELECT DISTINCT e.dorsal FROM etapa e  
      ) c1
      JOIN ciclista c
      USING (dorsal);

    -- director de los equipos
    SELECT e.director FROM (
      SELECT DISTINCT c.nomequipo FROM (
        SELECT DISTINCT e.dorsal FROM etapa e  
        ) c1
      JOIN ciclista c
      USING (dorsal)
      ) c2
      JOIN equipo e
      USING (nomequipo); 
    
-- 1.5 Dorsal y nombre de los ciclistas que hayan llevado algún maillot

    -- c1: ciclistas que han llevado algún maillot 
    SELECT DISTINCT l.dorsal FROM lleva l;
    
    SELECT dorsal, c.nombre FROM (
      SELECT DISTINCT l.dorsal FROM lleva l
      ) c1
      JOIN
      ciclista c
      USING (dorsal);

-- 1.6 Dorsal y nombre de los ciclistas que hayan llevado el maillot amarillo
  
    -- c1: codigo del maillot amarillo
    SELECT m.código FROM maillot m WHERE m.color = 'amarillo'; 

    -- c2: ciclistas que han llevado ese maillot
    SELECT DISTINCT l.dorsal FROM (
      SELECT m.código FROM maillot m WHERE m.color = 'amarillo'
      ) c1
      JOIN 
      lleva l
      USING (código);


    SELECT c.dorsal, c.nombre FROM (
      SELECT DISTINCT l.dorsal FROM (
        SELECT m.código FROM maillot m WHERE m.color = 'amarillo'
        ) c1
        JOIN 
        lleva l
        USING (código)
      ) c2
      JOIN 
      ciclista c
      USING (dorsal);

-- 1.7 Dorsal de los ciclistas que hayan llevado algún maillot y que han ganado etapas
    
    -- c1: ciclista que hayan llevado algún maillot
    SELECT DISTINCT l.dorsal FROM lleva l;

    -- c2: ciclistas que han ganado etapas
    SELECT DISTINCT e.dorsal FROM etapa e;

    SELECT dorsal FROM (
      SELECT DISTINCT l.dorsal FROM lleva l
      ) c1
      JOIN (
      SELECT DISTINCT e.dorsal FROM etapa e
      ) c2
      USING (dorsal);


-- 1.8 Indicar el numetapa de las etapas que tengan puertos
    
    SELECT DISTINCT p.numetapa FROM puerto p;

       
-- 1.9 Indicar los km de las etapas que hayan ganado ciclistas del Banesto y que tengan puertos
    
  -- c1: ciclistas del Banesto
    SELECT dorsal 
      FROM ciclista
    WHERE nomequipo='Banesto';

  -- c2: ciclistas del Banesto y que hayan ganado etapas
    SELECT e.numetapa 
      FROM etapa e 
    JOIN (
      SELECT dorsal 
      FROM ciclista
    WHERE nomequipo='Banesto'
      ) c1
    USING (dorsal);

  -- c3: etapas ganadas por ciclistas del Banesto y que tienen puertos 
    SELECT DISTINCT c2.numetapa 
      FROM puerto p
    JOIN (
      SELECT e.numetapa 
      FROM etapa e 
    JOIN (
      SELECT dorsal 
      FROM ciclista
    WHERE nomequipo='Banesto'
      ) c1
    USING (dorsal)
      ) c2 USING (numetapa); 

  -- km de esas etapas
    SELECT e.kms FROM (
      SELECT DISTINCT c2.numetapa 
      FROM puerto p
    JOIN (
      SELECT e.numetapa 
      FROM etapa e 
    JOIN (
      SELECT dorsal 
      FROM ciclista
    WHERE nomequipo='Banesto'
      ) c1
    USING (dorsal)
      ) c2 USING (numetapa)) c3
      JOIN etapa e USING(numetapa);

-- 1.10 Listar el número de ciclistas que hayan ganado alguna etapa con puerto
  
  /*
  -- con esto cuento los ciclistas que han ganado puertos
  SELECT COUNT(DISTINCT p.dorsal) FROM puerto p;
  */

  -- solucion 
  SELECT COUNT(DISTINCT e.dorsal) FROM etapa e JOIN puerto p USING (numetapa);
  

-- 1.11 Indicar el nombre de los puertos que hayan sido ganados por ciclistas de Banesto
  -- c1: ciclistas de Banesto
  SELECT c.dorsal FROM ciclista c WHERE c.nomequipo='Banesto';

  -- 
  SELECT 
    p.nompuerto 
  FROM (
    SELECT c.dorsal FROM ciclista c WHERE c.nomequipo='Banesto'
    ) c1
  JOIN 
    puerto p
  USING (dorsal);


-- 1.12 Listar el número de etapas que tengan puerto que hayan sido ganados por ciclistas de Banesto con más de 200 km

  -- c1: ciclistas de Banesto
  SELECT c.dorsal FROM ciclista c WHERE c.nomequipo='Banesto';

  -- c2: etapas que tengan más de 200 km y ciclistas que las han ganado
  SELECT e.dorsal, e.numetapa FROM etapa e WHERE e.kms>=200;


  -- c3: etapas que tengan más de 200 km y ciclistas de Banesto que las han ganado
  SELECT c2.numetapa FROM (
    SELECT c.dorsal FROM ciclista c WHERE c.nomequipo='Banesto'
    ) c1
    JOIN (
    SELECT e.dorsal, e.numetapa FROM etapa e WHERE e.kms>=200
    ) c2
    USING (dorsal);

  -- c4: etapas que tengan más de 200kms con puertos y ganadas por ciclistas de Banesto
  SELECT COUNT(*) FROM (
    SELECT c2.numetapa FROM (
    SELECT c.dorsal FROM ciclista c WHERE c.nomequipo='Banesto'
    ) c1
    JOIN (
    SELECT e.dorsal, e.numetapa FROM etapa e WHERE e.kms>=200
    ) c2
    USING (dorsal)
    ) c3
    JOIN
    puerto p 
    USING (numetapa);

  -- en una sola consulta combinando
  SELECT e.numetapa, e.kms FROM ciclista c JOIN etapa e USING (dorsal) JOIN puerto p USING (numetapa) WHERE c.nomequipo='Banesto' AND e.kms>=200;
